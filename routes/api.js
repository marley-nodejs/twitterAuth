var express = require('express');
var router = express.Router();

var config = require('../config');

var jwt = require('jsonwebtoken');
var moment = require('moment');
var User = require('../models/user');
var secretKey = config.TOKEN_SECRET;


function ensureAuthenticated(req, res, next) {
  var token = req.body.token || req.param('token') || req.headers['x-access-token'];
  if(token) {
    jwt.verify(token, secretKey, function(err, decoded) {
      if(err) {
        res.status(403).send({ message: "Failed to authorize the header"});
      }

        req.decoded = decoded;
        next();
    });

  } else {
    res.status(403).send({ message: "Failed to retrieve token"});
  }
}

function createToken(user) {

  var token = jwt.sign({
    id: user._id,
  }, secretKey, {
    expirtesInMinute: 1440
  });


  return token;

}


module.exports = function(app, express) {

  var router = express.Router();

  router.get('/me', ensureAuthenticated, function(req, res, next) {
    User.findById(req.decoded.id, function(err, user) {
     console.log(user);
     res.json(user);
   });
  });

  return router;

}
