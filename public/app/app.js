angular.module('MyApp', [ 'ngMessages','ngRoute', 'ui.router','satellizer'])
   
  .config(function($authProvider) {

         // Twitter
    $authProvider.authHeader = 'x-access-token';
    $authProvider.httpInterceptor = true; // Add Authorization header to HTTP request
    $authProvider.tokenPrefix = 'twitterAuth'; // Local Storage name prefix
    
    
    $authProvider.twitter({
      url: '/auth/twitter',
      type: '1.0',
      popupOptions: { width: 495, height: 645 }
    });


  })



  .config(function($stateProvider, $urlRouterProvider,$locationProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/views/home.html'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'app/views/login.html',
        controller: 'LoginCtrl'
      })
      .state('logout', {
        url: '/logout',
        template: null,
        controller: 'LogoutCtrl'
      })
      .state('profile', {
        url: '/profile',
        templateUrl: 'app/views/profile.html',
        controller: 'ProfileCtrl',
        resolve: {
          authenticated: function($q, $location, $auth) {
            var deferred = $q.defer();

            if (!$auth.isAuthenticated()) {
              $location.path('/login');
            } else {
              deferred.resolve();
            }

            return deferred.promise;
          }
        }
      });

    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
 

  })


